import sqlite3
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from tornado.concurrent import run_on_executor
from tornado import gen

engine = create_engine('sqlite:///test.db')
db = declarative_base()
db.metadata.bind = engine
Session = sessionmaker(bind=engine)
session = Session()


class Service(object):

    __model__ = None

    def _isinstance(self, model, raise_error=True):
        rv = isinstance(model, self.__model__)
        if not rv and raise_error:
            raise ValueError('%s is not of type %s' % (model, self.__model__))
        return rv

    def new(self, **kwargs):
        return self.__model__(**kwargs)

    @gen.coroutine
    def save(self, model):
        self._isinstance(model)
        session.add(model)
        try:
            session.commit()
        except IntegrityError as err:
            session.rollback()
            return None
        return model

    def get(self, id):
        return self.__model__.query.get(id)

    def get_all(self, *ids):
        return self.__model__.query.filter(self.__model__.id.in_(ids)).all

    @gen.coroutine
    def find(self, **kwargs):
        return session.query(self.__model__).filter_by(**kwargs)

    @gen.coroutine
    def create(self, **kwargs):
        create_obj =  yield self.save(self.new(**kwargs))
        return create_obj

    @gen.coroutine
    def first(self, **kwargs):
        find_ob = yield self.find(**kwargs)
        return find_ob.first()

    @gen.coroutine
    def delete(self, model):
        self._isinstance(model)
        session.delete(model)
        session.commit()
        return dict(state=True)