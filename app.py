import os
from tornado.httpserver import HTTPServer
from tornado.web import Application, StaticFileHandler
from tornado.ioloop import IOLoop
from model import RSASerice
from tornado import log
from handlers import (
    MainHandler,
    UserHandler,
    KeyHandler,
    CryptHandler,
    CrypotWebSocket
)

PATH = os.path.join(os.path.dirname(__file__), "static")


log.enable_pretty_logging()



def app():
    urls = [
        ("/", MainHandler),
        ("/user", UserHandler),
        ("/static", StaticFileHandler, dict(path=PATH)),
        ("/user/key/(?P<username>\w+)", KeyHandler),
        ("/user/key/state/(?P<username>\w+)", KeyHandler),
        ("/crypt", CryptHandler, dict(rsas=RSASerice)),
        ("/wscrypt", CrypotWebSocket)
    ]
    return Application(urls, debug=True, static_path=PATH)

def make_app():
    http_server = HTTPServer(app())
    http_server.listen(8000)
    http_server.start(0)
    IOLoop.current().start()


if __name__ == '__main__':
    make_app()