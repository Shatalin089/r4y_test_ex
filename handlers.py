from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler
from tornado import gen, log
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor
from model import UserService, RSASerice



MAX_WORKERS = 4

executor = ThreadPoolExecutor(max_workers=MAX_WORKERS)


class MainHandler(RequestHandler):

    SUPPORTED_METHODS = ["GET"]

    @gen.coroutine
    def get(self):
        self.render("templates/main.html", pub_key='', username='', for_encrypt='', for_decrypt='')


class UserHandler(RequestHandler):

    @gen.coroutine
    def post(self):
        username = self.get_body_argument("username", None)
        if username:
            us = UserService()
            user = yield us.first(username=username)
            if not user:
                user = yield us.create(username=username)
            self.write(dict(username=user.username, id=user.id))
        else:
            self.write(dict(error='Username not set'))


class KeyHandler(RequestHandler):


    SUPPORTED_METHODS = ["GET", "POST"]

    _thread_pool = executor

    @run_on_executor(executor="_thread_pool")
    def create_key(self, username):
        file_name = username + '_part_2.key'
        dirs = './keys/' + file_name
        rsas = RSASerice()
        keys = rsas.create_keys(dirs)
        return keys

    @gen.coroutine
    def check_state(self, user):
        if user.key:
            return dict(state=True)
        else:
            return dict(state=False)

    @gen.coroutine
    def get_or_create_key(self, user, us):
        if user:
            rsas = RSASerice()
            if user and not user.key:
                pub_key, priv_key_part_1, priv_key_part_2 = yield self.create_key(user.username)
                user.key = yield rsas.create(pub_key=pub_key, priv_key_part_1=priv_key_part_1, name_key_part_2=priv_key_part_2)
                yield us.save(user)
                key = user.key.pub_key.decode("utf-8")
            else:
                key = user.key.pub_key.decode("utf-8")
            user = user.username
        else:
            user = "User not found"
            key = "WTF Man?!!!"
        return dict(user=user, key=key)

    @gen.coroutine
    def post(self):
        username = self.get_argument('username', None)
        yield self.get(username)

    @gen.coroutine
    def get(self, username):
        us = UserService()
        user = yield us.first(username=username)
        if 'state' in self.request.uri and user:
            state = yield self.check_state(user)
            self.write(state)
        else:
            key = yield self.get_or_create_key(user, us)
            self.write(key)


class CryptHandler(RequestHandler):

    def initialize(self, rsas):
        self.rsas = rsas()

    @gen.coroutine
    def encrypt(self, user, msg):
        pub_key = user.key.pub_key
        enc_msg = self.rsas.encrypt(msg, pub_key)
        return enc_msg

    @gen.coroutine
    def decrypt(self, user, msg):
        dec_msg = self.rsas.decrypt(msg=msg, priv_key_part_1 = user.key.priv_key_part_1, priv_key_part_2 = user.key.name_key_part_2)
        return dec_msg

    @gen.coroutine
    def post(self):
        method = self.get_argument("method")
        username = self.get_argument("username")
        msg = self.get_argument("msg")
        us = UserService()
        user = yield us.first(username=username)
        if user and user.key:
            if method == 'encrypt' :
                result = yield self.encrypt(user, msg)
                print(result)
                self.write(dict(enc_msg=result.decode("utf-8")))
            elif method =='decrypt':
                result = yield self.decrypt(user, msg)
                self.write(dict(dec_msg=result))
        else:
            pass


class CrypotWebSocket(WebSocketHandler):

    def open(self, *args, **kwargs):
        print("WebSocket opened")

    def on_message(self, message):
        self.write_message(u"You said: " + message)

    def on_close(self):
        print("WebSocket closed")


