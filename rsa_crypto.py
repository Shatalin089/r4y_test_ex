import rsa
import base64
from rsa import PublicKey, PrivateKey
from rsa import encrypt as enc
from rsa import decrypt as dec

class RsaCrypto(object):

    def __init__(self, bit_lenght_key=4096):
        self._rsa = rsa
        self._length = bit_lenght_key
        self._thread_cnt = self._check_cnt_core()
        self.public_key = None
        self._privat_key = None

    def _check_cnt_core(self):
        try:
            import multiprocessing
            return multiprocessing.cpu_count()
        except (ImportError, NotImplementedError):
            return 1

    def _gen_poolsize(self):
        if self._thread_cnt > 3:
            return self._thread_cnt - 2
        else:
            return self._thread_cnt

    def _generate_key(self):
        self.public_key, self._privat_key = self._rsa.newkeys(self._length, self._gen_poolsize())
        return self.public_key, self._privat_key

    def gen_key(self):
        return self._generate_key()

    def get_public_key(self):
        if not self.public_key:
            self._generate_key()
        return self.public_key.save_pkcs1()

    def get_privat_key(self):
        if not self._privat_key:
            self._generate_key()
        return self._privat_key.save_pkcs1()

    def get_pub_key_to_save(self):
        return base64.standard_b64encode(self.get_public_key())

    def out_key_to_get(self, key):
        return base64.standard_b64decode(key)

    @staticmethod
    def encrypt(msg, key=None):
        if key:
            pub_key = PublicKey.load_pkcs1(key)
            msg = base64.standard_b64decode(msg)
            return base64.standard_b64encode(enc(msg, pub_key))
        return ''

    @staticmethod
    def decrypt(msg, key):
        if key:
            priv_key = PrivateKey.load_pkcs1(key)
            # msg = base64.standard_b64decode(msg)
            print(msg)
            dec_msg = dec(msg, priv_key)
            print(dec_msg)
            return base64.standard_b64encode()
        return ''

    def gen_secur_rsa_key(self):
        b64key = bytearray(base64.standard_b64encode(self.get_privat_key()))
        len_key = len(b64key)
        point_slice = len_key//2
        part_1, part_2 = b64key[:point_slice], b64key[point_slice:]
        return (part_1, part_2)

    @staticmethod
    def secur_part_to_key(part_1, part_2):
        all_key = part_1 + part_2
        priv_key = base64.standard_b64decode(all_key)
        return priv_key

    @staticmethod
    def save_to_file(key, parth_to_file):
        with open(parth_to_file, 'wb') as _file:
            _file.write(key)

    @staticmethod
    def read_to_file(part_to_file):
        with open(part_to_file, 'rb') as _file:
            return _file.read()
