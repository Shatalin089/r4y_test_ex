from sqlalchemy.orm import relationship
from sqlalchemy import Integer, String, Column, ForeignKey, Boolean
from core import db, Service, engine
from rsa_crypto import RsaCrypto
from tornado.concurrent import run_on_executor
from tornado import gen

class User(db):

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(255), unique=True)
    key = relationship('RSAKey', uselist=False, back_populates='user')

    def __repr__(self):
        return self.username


class UserService(Service):
    __model__ = User


class RSAKey(db):

    __tablename__ = 'rsa_pub'

    id = Column(Integer, primary_key=True)
    pub_key = Column(String, nullable=False)
    priv_key_part_1 = Column(String, nullable=False)
    name_key_part_2 = Column(String, nullable=False)
    state_gen_key = Column(Boolean, default=False)
    state_end_gen = Column(Boolean, default=False)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship('User', back_populates='key')


class RSASerice(Service):

    __model__ = RSAKey

    def create_keys(self, dir="./keys/"):
        rsa = RsaCrypto(4096)
        pub_key = rsa.get_public_key()
        priv_key_part_1, priv_key_part_2 = rsa.gen_secur_rsa_key()
        rsa.save_to_file(priv_key_part_2, dir)
        return pub_key, priv_key_part_1, dir

    def encrypt(self, msg, pub_key):
        return RsaCrypto.encrypt(msg, pub_key)

    def decrypt(self, **kwargs):
        msg = kwargs.get('msg')
        print(msg)
        priv_key_part_1 = kwargs.get('priv_key_part_1')
        priv_key_part_2 = RsaCrypto.read_to_file(kwargs.get('priv_key_part_2'))
        key = RsaCrypto.secur_part_to_key(priv_key_part_1, priv_key_part_2)
        return RsaCrypto.decrypt(msg, key)

def create_all(db, engine):
    db.metadata.create_all(engine)


if __name__ == '__main__':
    create_all(db, engine)